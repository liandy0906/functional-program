package com.kamfu.optionaldata.exercise06_09;


import com.kamfu.function.common.Function;
import com.kamfu.function.common.Supplier;

/**
 * 这样的解决方案对抛出异常的方法无效。编写一个 lift 方法，使其适用于抛
 * 出异常的方法
 *
 * @see #lift(Function)
 */
public abstract class Option<A> {

    @SuppressWarnings("rawtypes")
    private static Option none = new None();

    protected abstract A getOrThrow();

    public abstract A getOrElse(Supplier<A> defaultValue);

    public abstract <B> Option<B> map(Function<A, B> f);

    public <B> Option<B> flatMap(Function<A, Option<B>> f) {
        return map(f).getOrElse(Option::none);
    }

    public Option<A> orElse(Supplier<Option<A>> defaultValue) {
        return map(x -> this).getOrElse(defaultValue);
    }

    public Option<A> filter(Function<A, Boolean> f) {
        return flatMap(x -> f.apply(x) ? this : none());
    }

    private static class None<A> extends Option<A> {

        private None() {
        }

        @Override
        protected A getOrThrow() {
            throw new IllegalStateException("getOrThrow called on None");
        }

        @Override
        public A getOrElse(Supplier<A> defaultValue) {
            return defaultValue.get();
        }

        @Override
        public <B> Option<B> map(Function<A, B> f) {
            return none();
        }

        @Override
        public String toString() {
            return "None";
        }
    }

    private static class Some<A> extends Option<A> {

        private final A value;

        private Some(A a) {
            value = a;
        }

        @Override
        protected A getOrThrow() {
            return this.value;
        }

        public A getOrElse(Supplier<A> defaultValue) {
            return this.value;
        }

        public <B> Option<B> map(Function<A, B> f) {
            return new Some<>(f.apply(this.value));
        }

        @Override
        public String toString() {
            return String.format("Some(%s)", this.value);
        }
    }

    public static <A> Option<A> some(A a) {
        return new Some<>(a);
    }

    @SuppressWarnings("unchecked")
    public static <A> Option<A> none() {
        return none;
    }

    public static <A, B> Function<Option<A>, Option<B>> lift(Function<A, B> f) {
        return x -> {
            try {
                return x.map(f);
            } catch (Exception e) {
                return Option.none();
            }
        };
    }

    public static <A, B> Function<A, Option<B>> hlift(Function<A, B> f) {
        return x -> {
            try {
                return Option.some(x).map(f);
            } catch (Exception e) {
                return Option.none();
            }
        };
    }
}
