package com.kamfu.makingjavafunctional.example02;

import com.kamfu.function.common.Function;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用泛型提高重用性
 */
public class CollectionUtilities03 {


    /**
     * 将Integer列表元素的值增加20%
     *
     * @param values
     * @return
     */
    public static final<T,U> List<U> addTwentyPercent(List<T> values,Function<T,U> func) {
        List<U> result = new ArrayList<U>();
        for (int i = 0; i < values.size(); i++) {
            result.add(func.apply(values.get(i)));
        }
        return result;
    }
}
