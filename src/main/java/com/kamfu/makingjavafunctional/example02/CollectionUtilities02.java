package com.kamfu.makingjavafunctional.example02;

import com.kamfu.function.common.Function;

import java.util.ArrayList;
import java.util.List;

/**
 * 重用计算
 */
public class CollectionUtilities02 {

    private final static Function<Integer,Double> addTwentyPercent= value->value*1.2;
    /**
     * 将Integer列表元素的值增加20%
     *
     * @param values
     * @return
     */
    public static final List<Double> addTwentyPercent(List<Integer> values) {
        List<Double> result = new ArrayList<Double>();
        for (int i = 0; i < values.size(); i++) {
            result.add(addTwentyPercent.apply(values.get(i)));
        }
        return result;
    }
}
