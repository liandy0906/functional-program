package com.kamfu.makingjavafunctional.example02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CollectionUtilities01 {

    /**
     * 将Integer列表元素的值增加20%
     *
     * @param values
     * @return
     */
    public static final List<Double> addTwentyPercent(List<Integer> values) {
        List<Double> result = new ArrayList<Double>();
        for (int i = 0; i < values.size(); i++) {
            result.add(values.get(i) * 1.2);
        }
        return result;
    }
}
