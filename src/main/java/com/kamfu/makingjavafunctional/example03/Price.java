package com.kamfu.makingjavafunctional.example03;


public class Price {

    public static final Price ZERO = new Price(0.0);



    public final double value;

    private Price(double value) {
        this.value = value;
    }

    public static Price price(double value) {
        if (value <= 0) {
            throw new IllegalArgumentException("Price must be greater than 0");
        } else {
            return new Price(value);
        }
    }

    public Price add(Price that) {
        return price(this.value + that.value);
    }

    public Price mult(int count) {
        return price(this.value * count);
    }

    @Override
    public String toString() {
        return Double.toString(this.value);
    }
}
