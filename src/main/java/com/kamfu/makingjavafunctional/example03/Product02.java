package com.kamfu.makingjavafunctional.example03;

/**
 * 含值类型的领域对象
 */
public class Product02 {

    public final String name;
    public final Price price;
    public final Weight weight;

    public Product02(String name, Price price, Weight weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }
}
