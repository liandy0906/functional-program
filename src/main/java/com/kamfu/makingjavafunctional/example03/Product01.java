package com.kamfu.makingjavafunctional.example03;

/**
 * 传统的领域对象
 */
public class Product01 {

  private final String name;
  private final double price;
  private final double weight;

  public Product01(String name, double price, double weight) {
    this.name = name;
    this.price = price;
    this.weight = weight;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }

  public double getWeight() {
    return weight;
  }
}
