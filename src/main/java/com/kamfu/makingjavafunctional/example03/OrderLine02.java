package com.kamfu.makingjavafunctional.example03;

import com.kamfu.function.common.Function;

public class OrderLine02 {

  private final Product02 product;
  private final int count;
  /**
   * 函数变量：重量累计
   */
  public static Function<Weight, Function<OrderLine02, Weight>> weightSum = x -> y -> x.add(y.getWeight());
  /**
   * 函数变量：价格累加
   */
  public static Function<Price, Function<OrderLine02, Price>> priceSum = x -> y -> x.add(y.getAmount());
  public OrderLine02(Product02 product, int count) {
    this.product = product;
    this.count = count;
  }

  public Weight getWeight() {
    return this.product.weight.mult(this.count);
  }

  public Price getAmount() {
    return this.product.price.mult(this.count);
  }
}
