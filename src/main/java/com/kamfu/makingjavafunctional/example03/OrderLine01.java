package com.kamfu.makingjavafunctional.example03;

public class OrderLine01 {

    private Product01 product;
    private int count;

    public OrderLine01(Product01 product, int count) {
        super();
        this.product = product;
        this.count = count;
    }

    public Product01 getProduct() {
        return product;
    }

    public void setProduct(Product01 product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getWeight() {
        return this.product.getWeight() * this.count;
    }

    public double getAmount() {
        return this.product.getPrice() * this.count;
    }
}
