package com.kamfu.makingjavafunctional;


import com.kamfu.function.common.Effect;

public interface Result02<T> {

    void bind(Effect<T> success, Effect<String> failure);

    static <T> Result02<T> failure(String message) {
        return new Failure<>(message);
    }

    static <T> Result02<T> success(T value) {
        return new Success<>(value);
    }

    class Success<T> implements Result02<T> {

        private final T value;

        private Success(T t) {
            value = t;
        }

        @Override
        public void bind(Effect<T> success, Effect<String> failure) {
            success.apply(value);
        }
    }

    class Failure<T> implements Result02<T> {

        private final String errorMessage;

        private Failure(String s) {
            this.errorMessage = s;
        }

        @Override
        public void bind(Effect<T> success, Effect<String> failure) {
            failure.apply(errorMessage);
        }
    }
}
