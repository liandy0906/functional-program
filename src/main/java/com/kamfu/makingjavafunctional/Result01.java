package com.kamfu.makingjavafunctional;

public interface Result01 {

  class Success implements Result01 {}

  class Failure implements Result01 {

    private final String errorMessage;

    public Failure(String s) {
      this.errorMessage = s;
    }

    public String getMessage() {
      return errorMessage;
    }
  }
}
