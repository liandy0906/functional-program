package com.kamfu.introduction;


import com.kamfu.function.common.List;
import com.kamfu.function.common.Tuple;

import static com.kamfu.function.common.List.fill;


/**
 * 一次购买多个甜甜圈
 */
public class DonutShop04 {

    public static Tuple<Donut, Payment03> buyDonut(final CreditCard creditCard) {
        return new Tuple<>(new Donut(), new Payment03(creditCard, Donut.price));
    }

    public static Tuple<List<Donut>, Payment03> buyDonuts(final int quantity,
                                                          final CreditCard creditCard) {
        return new Tuple<>(fill(quantity, Donut::new),
                new Payment03(creditCard, Donut.price * quantity));
    }
}
