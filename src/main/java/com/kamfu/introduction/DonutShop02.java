package com.kamfu.introduction;


import com.kamfu.function.common.Tuple;

/**
 * 一个带无副作用的程序
 */
public class DonutShop02 {

    public static Tuple buyDonut(CreditCard creditCard) {
        Donut donut = new Donut();
        Payment01 payment=new Payment01(creditCard,Donut.price);
        return new Tuple(donut,payment);
    }
}
