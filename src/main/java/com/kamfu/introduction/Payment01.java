package com.kamfu.introduction;

/**
 * 支付记录
 */
public class Payment01 {

    public final CreditCard creditCard;
    public final int amount;

    public Payment01(CreditCard creditCard, int amount) {
        this.creditCard = creditCard;
        this.amount = amount;
    }
}
