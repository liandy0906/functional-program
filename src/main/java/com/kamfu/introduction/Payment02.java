package com.kamfu.introduction;

/**
 * 支付记录
 */
public class Payment02 {

    public final CreditCard creditCard;
    public final int amount;

    public Payment02(CreditCard creditCard, int amount) {
        this.creditCard = creditCard;
        this.amount = amount;
    }

    /**
     * 可以将多个支付记录合并
     * @param payment
     * @return
     */
    public Payment02 combine(Payment02 payment) {
        if (creditCard.equals(payment.creditCard)) {
            return new Payment02(creditCard, amount + payment.amount);
        } else {
            throw new IllegalStateException("Cards don't match.");
        }
    }
}
