package com.kamfu.introduction;

/**
 * 一个带有副作用的程序
 */
public class DonutShop01 {

    public static Donut buyDonut(CreditCard creditCard) {
        Donut donut = new Donut();
        //信用卡支付这是一个副作用
        creditCard.charge(Donut.price);
        return donut;
    }
}
