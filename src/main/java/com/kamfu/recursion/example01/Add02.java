package com.kamfu.recursion.example01;


/**
 *
 */
public class Add02 {

  static TailCall01<Integer> add(int x, int y) {
    return y == 0
        ? new TailCall01.Return<>(x)
        : new TailCall01.Suspend<>(() -> add(x + 2, y - 1));
  }
}
