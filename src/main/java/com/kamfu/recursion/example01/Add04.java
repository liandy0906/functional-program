package com.kamfu.recursion.example01;


import com.kamfu.function.common.Function;
import com.kamfu.function.common.TailCall;

import static com.kamfu.function.common.TailCall.ret;
import static com.kamfu.function.common.TailCall.sus;


/**
 * 独立的尾递归函数
 */
public class Add04 {

    static Function<Integer, Function<Integer, Integer>> add = x -> y -> {
        class AddHelper {
            Function<Integer, Function<Integer, TailCall<Integer>>> addHelper =
                    a -> b -> b == 0
                            ? ret(a)
                            : sus(() -> this.addHelper.apply(a + 1).apply(b - 1));
        }
        return new AddHelper().addHelper.apply(x).apply(y).eval();
    };
}
