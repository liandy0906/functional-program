package com.kamfu.recursion.example01;

import static com.kamfu.recursion.example01.TailCall01.*;

public class Add03 {

  static TailCall01<Integer> add(int x, int y) {
    return y == 0
        ? ret(x)
        : sus(() -> add(x + 1, y - 1));
  }
}
