package com.kamfu.recursion.example01;

/**
 * 这是一个简单的递归调用
 */
public class Add01 {

    static int add(int x, int y) {
        return y == 0 ? x : add(++x, --y);
    }
}
