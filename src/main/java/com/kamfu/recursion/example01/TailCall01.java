package com.kamfu.recursion.example01;

import com.kamfu.function.common.Supplier;

public abstract class TailCall01<T> {

    public abstract TailCall01<T> resume();

    public abstract T eval();

    public abstract boolean isSuspend();

    private TailCall01() {
    }

    public static class Return<T> extends TailCall01<T> {

        private final T t;

        public Return(T t) {
            this.t = t;
        }

        @Override
        public T eval() {
            return t;
        }

        @Override
        public boolean isSuspend() {
            return false;
        }

        @Override
        public TailCall01<T> resume() {
            throw new IllegalStateException("Return has no resume");
        }
    }

    public static class Suspend<T> extends TailCall01<T> {

        private final Supplier<TailCall01<T>> resume;

        public Suspend(Supplier<TailCall01<T>> resume) {
            this.resume = resume;
        }

        @Override
        public T eval() {
            TailCall01<T> tailRec = this;
            while (tailRec.isSuspend()) {
                tailRec = tailRec.resume();
            }
            return tailRec.eval();
        }

        @Override
        public boolean isSuspend() {
            return true;
        }

        @Override
        public TailCall01<T> resume() {
            return resume.get();
        }
    }

    public static <T> Return<T> ret(T t) {
        return new Return<>(t);
    }

    public static <T> Suspend<T> sus(Supplier<TailCall01<T>> s) {
        return new Suspend<>(s);
    }
}
