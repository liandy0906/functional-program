package com.kamfu.recursion.example03;

import java.util.List;

import com.kamfu.function.common.TailCall;

import static com.kamfu.function.common.CollectionUtilities.*;
import static com.kamfu.function.common.TailCall.*;

public class Range {

    public static List<Integer> range(Integer start, Integer end) {
        return range_(list(), start, end).eval();
    }

    private static TailCall<List<Integer>> range_(List<Integer> acc, Integer start, Integer end) {
        return end <= start
                ? ret(acc)
                : sus(() -> range_(append(acc, start), start + 1, end));

    }
}
