package com.kamfu.recursion.example03;

import com.kamfu.function.common.Function;


import java.util.List;

import static com.kamfu.recursion.example03.FoldLeft.foldLeft;
import static com.kamfu.recursion.example03.FoldRight.foldRight;
import static com.kamfu.function.common.CollectionUtilities.*;

public class ComposeAll03 {

    static <T> Function<T, T> composeAllViaFoldLeft(List<Function<T, T>> list) {
        return x -> foldLeft(reverse(list), x, a -> b -> b.apply(a));
    }

    static <T> Function<T, T> composeAllViaFoldRight(List<Function<T, T>> list) {
        return x -> foldRight(list, x, a -> a::apply);
    }

    static <T> Function<T, T> andThenAllViaFoldLeft(List<Function<T, T>> list) {
        return x -> foldLeft(list, x, a -> b -> b.apply(a));
    }

    static <T> Function<T, T> andThenAllViaFoldRight(List<Function<T, T>> list) {
        return x -> foldRight(reverse(list), x, a -> a::apply);
    }
}
