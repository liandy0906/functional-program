package com.kamfu.recursion.example03;

import com.kamfu.function.common.Function;
import com.kamfu.function.common.TailCall;

import java.util.List;


import static com.kamfu.function.common.CollectionUtilities.*;
import static com.kamfu.function.common.TailCall.*;


public class FoldLeft {

  public static <T, U> U foldLeft(List<T> ts, U identity, Function<U, Function<T, U>> f) {
    return foldLeft_(ts, identity, f).eval();
  }

  private static <T, U> TailCall<U> foldLeft_(List<T> ts, U identity, Function<U, Function<T, U>> f) {
    return ts.isEmpty()
        ? ret(identity)
        : sus(() -> foldLeft_(tail(ts), f.apply(identity).apply(head(ts)), f));
  }
}
