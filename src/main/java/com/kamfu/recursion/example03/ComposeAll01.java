package com.kamfu.recursion.example03;

import java.util.List;

import com.kamfu.function.common.Function;

import static com.kamfu.function.common.Function.identity;
import static com.kamfu.recursion.example03.FoldRight.foldRight;

public class ComposeAll01 {

    static <T> Function<T, T> composeAll(List<Function<T, T>> list) {
        return foldRight(list, identity(), x -> y -> x.compose(y));
    }

    /*
     * Since the functions are from T to T, composition may be implemented
     * using andThen instead of compose.
     */
    static <T> Function<T, T> composeAllViaAndThen(List<Function<T, T>> list) {
        return foldRight(list, identity(), x -> y -> y.andThen(x));
    }
}
