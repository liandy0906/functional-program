package com.kamfu.recursion.example03;

import java.util.List;

import com.kamfu.function.common.Function;
import com.kamfu.function.common.TailCall;

import static com.kamfu.function.common.CollectionUtilities.*;
import static com.kamfu.function.common.TailCall.*;

public class FoldRight {

    public static <T, U> U foldRight(List<T> ts, U identity, Function<T, Function<U, U>> f) {
        return foldRight_(identity, reverse(ts), f).eval();
    }

    private static <T, U> TailCall<U> foldRight_(U acc, List<T> ts, Function<T, Function<U, U>> f) {
        return ts.isEmpty()
                ? ret(acc)
                : sus(() -> foldRight_(f.apply(head(ts)).apply(acc), tail(ts), f));
    }
}
