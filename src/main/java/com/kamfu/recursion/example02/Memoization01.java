package com.kamfu.recursion.example02;


import com.kamfu.function.common.Function;

public class Memoization01 {

  public static void main(String[] args) {
    automaticMemoizationExample();
  }

  private static Integer longCalculation(Integer x) {
    try {
      Thread.sleep(1_000);
    } catch (InterruptedException ignored) {
    }
    return x * 2;
  }

  private static Function<Integer, Integer> f = Memoization01::longCalculation;
  private static Function<Integer, Integer> g = Memoizer.memoize(f);

  public static void automaticMemoizationExample() {
    long startTime = System.currentTimeMillis();
    Integer result1 = g.apply(1);
    long time1 = System.currentTimeMillis() - startTime;
    startTime = System.currentTimeMillis();
    Integer result2 = g.apply(1);
    long time2 = System.currentTimeMillis() - startTime;
    System.out.println(String.format("result1:%s",result1));
    System.out.println(String.format("result2:%s",result2));
    System.out.println(String.format("time1:%s",time1));
    System.out.println(String.format("time2:%s",time2));
  }
}
