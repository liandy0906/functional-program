package com.kamfu.function.common;

/**
 * 作用
 *
 * @param <T>
 */
public interface Effect<T> {
    void apply(T t);
}
