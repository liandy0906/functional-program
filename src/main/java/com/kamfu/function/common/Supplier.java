package com.kamfu.function.common;

public interface Supplier<T> {
    T get();
}
