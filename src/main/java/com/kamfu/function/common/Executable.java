package com.kamfu.function.common;

/**
 * 可执行接口类
 */
public interface Executable {
    void exec();
}
