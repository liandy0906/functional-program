package com.kamfu.function.state;


import com.kamfu.function.common.Tuple;

public interface RNG {

  Tuple<Integer, RNG> nextInt();
}
