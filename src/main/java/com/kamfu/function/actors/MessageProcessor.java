package com.kamfu.function.actors;


import com.kamfu.function.common.Result;

public interface MessageProcessor<T> {

  void process(T t, Result<Actor<T>> sender);
}
