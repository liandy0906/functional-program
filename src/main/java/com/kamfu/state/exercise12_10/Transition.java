package com.kamfu.state.exercise12_10;


import com.kamfu.function.common.Function;

public interface Transition<A, S> extends Function<StateTuple<A, S>, S> {}
