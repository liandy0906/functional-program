package com.kamfu.state.exercise12_10;


import com.kamfu.function.common.Function;

public interface Condition<I, S> extends Function<StateTuple<I, S>, Boolean> {}
