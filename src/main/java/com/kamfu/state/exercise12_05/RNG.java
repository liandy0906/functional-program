package com.kamfu.state.exercise12_05;


import com.kamfu.function.common.Tuple;

public interface RNG {
  Tuple<Integer, RNG> nextInt();
}
