package com.kamfu.state.exercise12_08;


import com.kamfu.function.common.Tuple;

public interface RNG {
  Tuple<Integer, RNG> nextInt();
}
