package com.kamfu.state.exercise12_07;


import com.kamfu.function.common.Tuple;

public interface RNG {
  Tuple<Integer, RNG> nextInt();
}
