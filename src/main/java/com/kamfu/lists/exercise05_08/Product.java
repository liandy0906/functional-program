package com.kamfu.lists.exercise05_08;

import com.kamfu.lists.exercise05_06.List;

/**
 * 编写 个函数式方法，使用基于拢的递归来计算双精度列表内所有元素的乘积。
 */
public class Product {

    public static Double product(List<Double> ints) {
        return ints.isEmpty() ? 1 : ints.head() * product(ints.tail());
    }
}
