package com.kamfu.lists.exercise05_07;


import com.kamfu.lists.exercise05_06.List;

/**
 * 编写 个函数式方法，使用简单的基于校的递归来计算整型列表内所有元素的
 * 总和。
 */
public class Sum {

    public static Integer sum(List<Integer> ints) {
        return ints.isEmpty() ? 0 : ints.head() + sum(ints.tail());
    }
}
