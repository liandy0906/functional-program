package com.kamfu.lists.exercise05_12;


import com.kamfu.lists.exercise05_10.List;

import static com.kamfu.lists.exercise05_10.List.list;

/**
 * 使用 foldLeft 编写一个用于反转列表的静态函数式方法。
 */
public class Reverse {

  public static <A> List<A> reverseViaFoldLeft(List<A> list) {
    return list.foldLeft(list(), x -> x::cons);
  }
}
