package com.kamfu.usingfunctions.exercise02_00;

public interface Function {

  int apply(int arg);
}
