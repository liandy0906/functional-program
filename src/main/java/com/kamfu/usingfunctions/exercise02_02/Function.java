package com.kamfu.usingfunctions.exercise02_02;

public interface Function<T, U> {

  U apply(T arg);
}
