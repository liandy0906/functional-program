package com.kamfu.usingfunctions.exercise02_04;

public interface Function<T, U> {

  U apply(T arg);

}
