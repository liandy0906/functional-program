package com.kamfu.usingfunctions.exercise02_01;

public interface Function<T, U> {

  U apply(T arg);
}
