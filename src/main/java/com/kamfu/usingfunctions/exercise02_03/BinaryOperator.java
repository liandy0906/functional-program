package com.kamfu.usingfunctions.exercise02_03;


import com.kamfu.usingfunctions.exercise02_02.Function;

public interface BinaryOperator extends Function<Integer, Function<Integer, Integer>> {

}
