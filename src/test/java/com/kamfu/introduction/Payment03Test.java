package com.kamfu.introduction;

import static org.junit.Assert.*;

import org.junit.Test;

import com.kamfu.function.common.List;
import com.kamfu.function.common.Map;
import com.kamfu.function.common.Tuple;

public class Payment03Test {

    @Test
    public void testGroupByCard() {
        CreditCard creditCard1 = new CreditCard();
        CreditCard creditCard2 = new CreditCard();
        Tuple<List<Donut>, Payment03> purchase1 = DonutShop04.buyDonuts(5, creditCard1);
        Tuple<List<Donut>, Payment03> purchase2 = DonutShop04.buyDonuts(3, creditCard2);
        Tuple<List<Donut>, Payment03> purchase3 = DonutShop04.buyDonuts(2, creditCard1);
        Tuple<List<Donut>, Payment03> purchase4 = DonutShop04.buyDonuts(1, creditCard1);
        Tuple<List<Donut>, Payment03> purchase5 = DonutShop04.buyDonuts(4, creditCard2);
        List<Payment03> paymentList = Payment03.groupByCard(List.list(purchase1._2, purchase2._2, purchase3._2, purchase4._2, purchase5._2));
        assertEquals(2, paymentList.length());
        Map<CreditCard, Integer> payments = new Map<>();
        paymentList.foldLeft(payments, ps -> p -> ps.put(p.creditCard, p.amount));
        assertTrue(payments.get(creditCard1).map(v -> v.equals(16)).getOrElse(false));
        assertTrue(payments.get(creditCard2).map(v -> v.equals(14)).getOrElse(false));
    }
}
