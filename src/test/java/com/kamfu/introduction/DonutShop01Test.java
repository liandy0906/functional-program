package com.kamfu.introduction;


import static org.junit.Assert.*;

import org.junit.Test;



public class DonutShop01Test {

  @Test
  public void testBuyCoffee() {
    CreditCard crediCard = new CreditCard();
    DonutShop01.buyDonut(crediCard);
    DonutShop01.buyDonut(crediCard);
    assertEquals(Donut.price * 2, crediCard.getTotal());
  }

}
