package com.kamfu.introduction;


import com.kamfu.function.common.List;
import com.kamfu.function.common.Tuple;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class Donutshop03Test {
    @Test
    public void testBuyDonuts() {
        CreditCard creditCard = new CreditCard();
        Tuple<List<Donut>, Payment02> purchase = DonutShop03.buyDonuts(5, creditCard);
        assertEquals(Donut.price * 5, purchase._2.amount);
        assertEquals(creditCard, purchase._2.creditCard);
    }
}
