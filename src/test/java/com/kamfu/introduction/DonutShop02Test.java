package com.kamfu.introduction;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DonutShop02Test {

  @Test
  public void testBuyCoffee() {
    CreditCard crediCard = new CreditCard();
    DonutShop02.buyDonut(crediCard);
    DonutShop02.buyDonut(crediCard);
    assertEquals(Donut.price * 2, crediCard.getTotal());
  }

}
