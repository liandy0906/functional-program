package com.kamfu.makingjavafunctional.example02;

import com.kamfu.function.common.Function;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: CollectionUtilities02Test
 * @Author: liandy
 * @Date: 2022/9/18 10:40
 * @Description: TODO
 */
public class CollectionUtilities03Test {
    @Test
    public void test(){
        Function<Integer,Double> addFun= value->value*1.2;
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < 100;i++) {
            values.add(Integer.valueOf(i));
        }
        List<Double> result= CollectionUtilities03.addTwentyPercent(values,addFun);
        Assert.assertEquals(values.stream().map(value->value*1.2).collect(Collectors.toList()),result);
    }
}
