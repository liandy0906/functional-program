package com.kamfu.makingjavafunctional.example02;
import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * @ClassName: CollectionUtilities02Test
 * @Author: liandy
 * @Date: 2022/9/18 10:40
 * @Description: TODO
 */
public class CollectionUtilities02Test {
    @Test
    public void test(){
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < 100;i++) {
            values.add(Integer.valueOf(i));
        }
        List<Double> result= CollectionUtilities02.addTwentyPercent(values);
        Assert.assertEquals(values.stream().map(value->value*1.2).collect(Collectors.toList()),result);
    }
}
