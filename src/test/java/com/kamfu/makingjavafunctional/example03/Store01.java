package com.kamfu.makingjavafunctional.example03;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 命令式代码
 */
public class Store01 {

    @Test
    public void test() {
        Product01 toothPaste = new Product01("Tooth paste", 1.5, 0.5);
        Product01 toothBrush = new Product01("Tooth brush", 3.5, 0.3);

        List<OrderLine01> order = new ArrayList<>();
        order.add(new OrderLine01(toothPaste, 2));
        order.add(new OrderLine01(toothBrush, 3));

        double price = 0.0;
        double weight = 0.0;
        for (OrderLine01 orderLine : order) {
            weight += orderLine.getAmount();
            price += orderLine.getWeight();
        }
        System.out.println(String.format("Total price: %s", price));
        System.out.println(String.format("Total weight: %s", weight));
    }
}
