package com.kamfu.makingjavafunctional.example03;


import org.junit.Test;

import java.util.List;

import static com.kamfu.makingjavafunctional.example02.CollectionUtilities09.*;

/**
 * 函数式代码
 */
public class Store02 {
    @Test
    public void test() {
        Product01 toothPaste = new Product01("Tooth paste", 1.5, 0.5);
        Product01 toothBrush = new Product01("Tooth brush", 3.5, 0.3);

        List<OrderLine01> order = list(
                new OrderLine01(toothPaste, 2),
                new OrderLine01(toothBrush, 3));

        double weight = foldLeft(order, 0.0, x -> y -> x + y.getAmount());
        double price = foldLeft(order, 0.0, x -> y -> x + y.getWeight());

        System.out.println(String.format("Total price: %s", price));
        System.out.println(String.format("Total weight: %s", weight));
    }
}
