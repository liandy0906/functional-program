package com.kamfu.makingjavafunctional.example03;


import java.util.List;

import static com.kamfu.makingjavafunctional.example02.CollectionUtilities09.foldLeft;
import static com.kamfu.makingjavafunctional.example02.CollectionUtilities09.list;
import static com.kamfu.makingjavafunctional.example03.Price.price;
import static com.kamfu.makingjavafunctional.example03.Weight.weight;

/**
 * 值类型代码
 */
public class Store03 {

    public static void main(String[] args) {

        Product02 toothPaste = new Product02("Tooth paste", price(1.5), weight(0.5));
        Product02 toothBrush = new Product02("Tooth brush", price(3.5), weight(0.3));

        List<OrderLine02> order = list(
                new OrderLine02(toothPaste, 2),
                new OrderLine02(toothBrush, 3));

        Price price = foldLeft(order, Price.ZERO, OrderLine02.priceSum);
        Weight weight = foldLeft(order, Weight.ZERO, OrderLine02.weightSum);

        System.out.println(String.format("Total price: %s", price));
        System.out.println(String.format("Total weight: %s", weight));

    }
}
