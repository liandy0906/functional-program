package com.kamfu.usingfunctions;

import static com.kamfu.usingfunctions.ComposingFunctionsStackOverflow.*;

import org.junit.Test;

public class ComposingFunctionsStackOverflowTest {

  @Test(expected=StackOverflowError.class)
  public void test() {
      g.apply(0);
  }
}
