package com.kamfu.lambda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.awt.event.ActionListener;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Unit test for simple App.
 */
public class LambdaTest {
    /**
     * lambda表达式不同形式
     */
    @Test
    public void lambdaExpress() {
        //1无参无返回类型
        Runnable noArguments = () -> System.out.println("lambda");

        //2一个参数无返回类型
        ActionListener oneArguments = event -> System.out.println(event.getActionCommand());

        //3无参无返回类型多行语句
        Runnable multiStatement = () -> {
            System.out.println("1");
            System.out.println("2");
        };

        //4多个隐式类型参数有返回类型
        BinaryOperator<Long> add = (x, y) -> x + y;

        //5多个显式类型参数有返回类型
        BinaryOperator<Long> addExplicit = (Long x, Long y) -> x + y;
    }

    /**
     * 流
     */
    @Test
    public void stream() {
        //1.collect：生成列表
        List<String> collected = Stream.of("a", "b", "c").collect(Collectors.toList());
        assertEquals(Arrays.asList("a", "b", "c"), collected);

        //2.map：一对一的流类型转换
        List<String> mapList = Stream.of("a", "b", "c")
                .map(str -> str.toUpperCase())
                .collect(Collectors.toList());
        assertEquals(Arrays.asList("A", "B", "C"), mapList);

        //3.filter：元素过滤
        List<String> lowerCaseList = Stream.of("a", "B", "c")
                .filter(str -> Character.isLowerCase(str.charAt(0)))
                .collect(Collectors.toList());
        assertEquals(Arrays.asList("a", "c"), lowerCaseList);
        List<String> digitList = Stream.of("1", "B", "a", "3")
                .filter(str -> Character.isDigit(str.charAt(0)))
                .collect(Collectors.toList());
        assertEquals(Arrays.asList("1", "3"), digitList);

        //4.flatMap：对流的元素一对多的转换
        List<Album> albumList = SampleData.getThreeAlbums();
        List<Track> trackList = albumList.stream()
                .flatMap(album -> album.getTracks())
                .collect(Collectors.toList());
        assertEquals(8, trackList.size());

        //5.max、min：最大值、最小值的流元素
        Track maxLengthTrack = albumList.stream()
                .flatMap(album -> album.getTracks())
                .max(Comparator.comparing(track -> track.getLength()))
                .get();
        assertEquals(467, maxLengthTrack.getLength());
        Track minLengthTrack = albumList.stream()
                .flatMap(album -> album.getTracks())
                .max(Comparator.comparing(track -> -track.getLength()))
                .get();
        assertEquals(30, minLengthTrack.getLength());

        //6.reduce：约简运算
        int count = Stream.of(1, 2, 3, 4)
                .reduce(-8, (acc, e) -> acc + e);
        assertEquals(2, count);

        //7.找出专辑上所有乐队国籍（假定乐队名以定冠词The开头）
        Set<String> nationals = SampleData.getTheBeatlesAlbum().stream()
                .flatMap(album -> album.getMusicians())
                .filter(artist -> artist.getName().startsWith("The"))
                .map(artist -> artist.getNationality())
                .collect(Collectors.toSet());
        assertEquals(new HashSet<>(Arrays.asList("UK")), nationals);

        //8.找出长度大于60的曲目
        List<Track> tracks = SampleData.getThreeAlbums().stream()
                .flatMap(albums -> albums.getTracks())
                .filter(track -> track.getLength() > 60)
                .collect(Collectors.toList());
        assertEquals(2, tracks.size());
    }

    /**
     * 高级集合类和收集器
     */
    @Test
    public void advanceCollecton() {
        //1.方法引用：ClassName::methodName
        List<String> artistNames = SampleData.getThreeArtists().stream()
                .map(Artist::getName)
                .collect(Collectors.toList());
        assertEquals(3, artistNames.size());


        //2.元素顺序
        //顺序测试永远通过
        List<Integer> numbers = Arrays.asList(4, 3, 2, 1);
        List<Integer> sameOrderNumbers = numbers.stream().collect(Collectors.toList());
        assertEquals(numbers, sameOrderNumbers);
        //顺序测试不能保证每次通过
//        Set<Integer> numberSet=new HashSet<>(numbers);
//        List<Integer> sameOrderNumberSet=numberSet.stream().collect(Collectors.toList());
//        assertEquals(Arrays.asList(4,3,2,1) , sameOrderNumberSet);


        //3.使用收集器
        //3.1转换成其它集合：toCollection toList……
        Set set = Arrays.asList(4, 3, 2, 1).stream().collect(Collectors.toCollection(TreeSet::new));
        assertEquals(4, set.size());

        //3.2转换成值：maxBy minBy averagingInt，找出成员最多的乐队
        Artist maxNumberArtist = SampleData.getThreeArtists().stream()
                .collect(Collectors.maxBy(Comparator.comparing(artist -> artist.getMembers().count()))).get();
        assertEquals("The Beatles", maxNumberArtist.getName());

        //3.3数据分块：partitioningBy 实现艺术家分成乐队和独唱两部分
        Map<Boolean, List<Artist>> map= SampleData.getThreeArtists().stream()
                .collect(Collectors.partitioningBy(Artist::isSolo));
        assertEquals(2, map.size());

        //3.4数据分组：groupingBy 按主唱音乐家对专辑分组
        Map<Artist, List<Album>> map1 = SampleData.getThreeAlbums().stream()
                .collect(Collectors.groupingBy(album -> album.getMainMusician()));
        assertEquals(2, map1.size());

        //3.5将元素按指定分割符分割并输出
        String result=SampleData.getThreeAlbums().stream()
                .map(Album::getName)
                .collect(Collectors.joining(",","[","]"));


        //3.6组合收集器
        // counting：计数收集器 计算每个艺术家专辑数量
        Map<Artist, Long> albumCountPerArtist=SampleData.getThreeAlbums().stream()
                .collect(Collectors.groupingBy(album -> album.getMainMusician(),Collectors.counting()));
        //mapping：映射收集器 求每个艺术家的专辑名
        Map<Artist, List<String>> albumNamesPerArtist=SampleData.getThreeAlbums().stream()
                .collect(Collectors.groupingBy(album -> album.getMainMusician(),Collectors.mapping(Album::getName,Collectors.toList())));

        //3.7定制收集器 TODO


    }

    /**
     * 数据并行化
     */
    @Test
    public void dataParallel(){
        //串行化计算机专辑曲目长度
        int trackLengthCountSerial=SampleData.getThreeAlbums().stream().flatMap(album -> album.getTracks()).mapToInt(Track::getLength).sum();

        //并行化计算专辑曲目长度
        int trackLengthCountParallel=SampleData.getThreeAlbums().parallelStream().flatMap(album -> album.getTracks()).mapToInt(Track::getLength).sum();
        assertEquals(trackLengthCountSerial,trackLengthCountParallel);

        //并行化数组操作
        double[] values=new double[10];
        Arrays.parallelSetAll(values,i->i*i);

    }

}
