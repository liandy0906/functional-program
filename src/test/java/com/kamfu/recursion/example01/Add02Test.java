package com.kamfu.recursion.example01;

import com.kamfu.recursion.example01.Add02;
import com.kamfu.recursion.example01.TailCall01;
import org.junit.Assert;
import org.junit.Test;

/**
 * @ClassName: Add02Test
 * @Author: liandy
 * @Date: 2022/9/18 16:45
 * @Description: TODO
 */
public class Add02Test {
    @Test
    public void test(){
        TailCall01<Integer> tailCall= Add02.add(3,10);
        while(tailCall.isSuspend()){
            tailCall=tailCall.resume();
        }
        Assert.assertEquals(13,tailCall.eval().intValue());
    }
}
